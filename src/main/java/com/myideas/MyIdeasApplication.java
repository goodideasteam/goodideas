package com.myideas;
/**
 * MyIdeas project is an innovative application to collect your ideas wherever you are, 
 * and then you can further discuss them in a braintorming session.
 *
 *
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyIdeasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyIdeasApplication.class, args);
	}
}
